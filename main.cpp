#include <iostream>
#include <time.h>
using namespace std;

#define N 23
#define N 5

int main()
{
	int A[N][N]{};
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			A[i][j] = i + j;
			cout.width(3);
			cout << A[i][j] << ' ';
		}
		cout << endl;
	}
	cout << endl;

	int sum = 0;
	struct tm buf;
	time_t t = time(NULL);
	int day = localtime_s(&buf, &t);
	buf.tm_mday;

	for (int x = 0; x < N; x++)
	{
		sum += A[day % N][x];
	}
	cout << sum << endl;

	return 0;
}
